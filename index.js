'use strict'
//carga Monggose:
var mongoose=require('mongoose');
//carga configuración de Express:
var app=require('./app');
var port=3800;


//conexión a la BBDD
mongoose.Promise=global.Promise;
mongoose.connect('mongodb://localhost:27017/red_social_MEAN', { useNewUrlParser: true })
    .then(()=>{
        console.log("La conexión a la BBDD se ha realizado correctamente");
        //crear servidor
        app.listen(port,()=>{
            console.log("Servidor corriendo en http://localhost:3800");
        });

    })
    .catch(err=>console.log(err));
