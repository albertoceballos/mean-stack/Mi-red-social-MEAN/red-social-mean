'use strict'

var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoose_pagination = require('mongoose-pagination');

//modelos
var Publication = require('../models/publication');
var User = require('../models/user');
var Follow = require('../models/follow');


//Crear nuevas publicaciones
function savePublication(req, res) {

    var params = req.body;
    var publication = new Publication();

    if (!params.text) {
        return res.status(200).send({ message: 'Debes enviar un texto' });
    }

    publication.text = params.text;
    publication.file = null;
    publication.user = req.user.sub;
    publication.created_at = moment().unix();

    publication.save((err, publicationStored) => {
        if (err) return res.status(500).send({ message: 'Error al guardar la publicación' });

        if (!publicationStored) return res.status(404).send({ message: 'La publicación no ha sido guardada' });

        return res.status(200).send({ publication: publicationStored });
    });
}

//publicaciones de los usuarios q los que sigo
function getPublications(req, res) {
    //página por defecto:
    var page = 1;

    //si la ruta incluye el parámetro de página se lo paso a la variable page
    if (req.params.page) {
        page = req.params.page;
    }

    //número de publicaciones por páginas por defecto:
    var itemsPerPage = 4;

    /*Saco los usuarios a los que sigo, hago un populate de followed pra que me rellene con
    todo el objeto del usuario seguido */

    Follow.find({ user: req.user.sub }).populate('followed').exec((err, follows) => {
        if (err) return res.status(500).send({ message: 'Error al devolver el seguimiento' });

        //hago un array que relleno con todos los objetos de usuarios a los que sigo
        var follows_array = [];
        follows.forEach(follow => {
            follows_array.push(follow.followed);
        });

        follows_array.push(req.user.sub);
        //Ahora busco las publicaciones que pertenecen a alguno de los usuarios en el array de usuarios que sigo,
        //hago la paginación
        Publication.find({ user: { "$in": follows_array } })
            .sort('-created_at')
            .populate('user')
            .paginate(page, itemsPerPage, (err, publications, total) => {
                if (err) return res.status(500).send({ message: 'Error al devolver las publicaciones' });

                if (!publications) return res.status(404).send({ message: 'No hay publicaciones' });

                return res.status(200).send({
                    total: total,
                    pages: Math.ceil(total / itemsPerPage),
                    page: page,
                    publications: publications,
                });
            });


    });
}

//Sacar una publicación por su Id
function getPublication(req, res) {
    var publication_id = req.params.id;

    Publication.findById(publication_id, (err, publication) => {
        if (err) return res.status(500).send({ message: "Error al extraer la publicación" });

        if (!publication) return res.status(404).send({ message: "No existe la publicación" });

        return res.status(200).send({ publication });
    });

}

//Borrar una publicación
function deletePublication(req, res) {
    var publication_id = req.params.id;

    Publication.findOneAndRemove({ 'user': req.user.sub, '_id': publication_id }, (err, publicationRemoved) => {
        if (err) return res.status(500).send({ message: 'Error al borrar la publicación' });
        if (!publicationRemoved) return res.status(404).send({ message: 'La publicación no existe' });
        return res.status(200).send({ message: 'La publicación fue eliminada con éxito' });
    });


}

function uploadImage(req, res) {
    //recojo el id de la publicación mandadado por la url
    var publicationId = req.params.id;

    //si se sube un archivo:
    if (req.files.image) {
        let file_path = req.files.image.path;
        let file_split = file_path.split('\\');
        //console.log(file_split);
        let image_name = file_split[2];
        //console.log(image_name);
        let image_name_split = image_name.split('\.');
        //console.log(image_name_split);
        let image_extension = image_name_split[1];
        console.log(image_extension);
        if (image_extension == 'jpg' || image_extension == 'jpeg' || image_extension == 'gif' || image_extension == 'png') {

            Publication.findOne({ '_id': publicationId, 'user': req.user.sub }, (err, publication) => {

                if (err) return res.status(500).send({ message: "Error al buscar publicación" });

                if (publication) {
                    Publication.findByIdAndUpdate(publicationId, { file: image_name }, { new: true }, (err, publicationUpdated) => {
                        if (err) return res.status(500).send({ message: 'Error en la petición' });

                        if (!publicationUpdated) return res.status(404).send({ message: 'No se ha podido actualizar el usuario' });

                        return res.status(200).send({ publication: publicationUpdated });
                    });
                } else {
                    fs.unlink(file_path, (error) => {
                        return res.status(500).send({ message: 'No tienes permiso para actualizar esta publicación' });
                    });
                }
            });


        } else {
            fs.unlink(file_path, (error) => {
                return res.status(500).send({ message: 'Extensión no válida' });
            });
        }
    } else {
        return res.status(404).send({ message: 'no se ha subido una imagen' });
    }

}

function getImageFile(req, res) {
    let image_file = req.params.imageFile;
    let path_file = './uploads/publications/' + image_file;

    fs.exists(path_file, (exists) => {
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            res.status(200).send({ message: 'No existe la imagen' });
        }
    });

}


function getPublicationsUser(req, res) {
    //página por defecto:
    var page = 1;

    //si la ruta incluye el parámetro de página se lo paso a la variable page
    if (req.params.page) {
        page = req.params.page;
    }

    var userId = req.user.sub;
    if (req.params.id) {
        userId = req.params.id;
    }

    //número de publicaciones por páginas por defecto:
    var itemsPerPage = 4;

    /*Saco los usuarios a los que sigo, hago un populate de followed pra que me rellene con
    todo el objeto del usuario seguido */

    //hago la paginación
    Publication.find({ user: userId })
        .sort('-created_at')
        .populate('user')
        .paginate(page, itemsPerPage, (err, publications, total) => {
            if (err) return res.status(500).send({ message: 'Error al devolver las publicaciones' });

            if (!publications) return res.status(404).send({ message: 'No hay publicaciones' });

            return res.status(200).send({
                total: total,
                pages: Math.ceil(total / itemsPerPage),
                page: page,
                publications: publications,
            });
        });

}


module.exports = {
    savePublication,
    getPublications,
    getPublication,
    deletePublication,
    uploadImage,
    getImageFile,
    getPublicationsUser,
}