'use strict'

//cargar paginación de mongoose
var mongoosePaginate = require('mongoose-pagination');

//librerias para uso de arachivos
var path = require('path');
var fs = require('fs');

//modelos
var User = require('../models/user');
var Follow = require('../models/follow');


//Guardar follow
function saveFollow(req, res) {
    let params = req.body;

    let follow = new Follow();

    //el usuario que sigue, es el usuario logueado que ya le tengo en req gracias al método para autenticar token:
    follow.user = req.user.sub;

    //el usuario a seguir es el usuario que tengo en params y le paso por POST (req.body):
    follow.followed = params.followed;

    follow.save((err, followStored) => {
        if (err) return res.status(500).send({ message: 'Error en el servidor al guardar el follow' });

        if (!followStored) return res.status(404).send('El follow no se ha guardado');

        return res.status(200).send({ follow: followStored });
    });


}

//Borrar follow
function deleteFollow(req, res) {

    let userId = req.user.sub;

    //recojo el Id del usuario que sigo y al que quiero dejar de seguir que le paso por GET
    let followedId = req.params.id;

    Follow.find({ 'user': userId, 'followed': followedId }).remove((err) => {
        if (err) return res.status(500).send({ message: 'Error al eliminar el follow' });

        return res.status(200).send({ message: 'Has dejado de seguir al usuario' });
    });

}


//usuarios que sigue un usuario
function getFollowingUsers(req, res) {
    var userId = req.user.sub;
    var page = 1;
    if (req.params.id && req.params.page) {
        userId = req.params.id;
    }

    if (req.params.page) {
        page = req.params.page;
    }

    var itemsPerPage = 4;
    Follow.find({ user: userId })
        .populate({ path: 'followed' })
        .paginate(page, itemsPerPage, (err, follows, total) => {
            if (err) res.status(500).send({ message: 'Error en el servidor' });
            if (!follows) {
                return res.status(404).send({ message: 'No estás siguiendo a ningun usuario' });
            }

            followUserId(req.user.sub).then((value) => {
                return res.status(200).send({
                    total: total,
                    pages: Math.ceil(total / itemsPerPage),
                    follows,
                    users_followed:value.followed,
                    users_following:value.following
                });
            });
        });
}

async function followUserId(user_id) {
    try {
        var following = await Follow.find({ "user": user_id }).select({ '_id': 0, '__v': 0, 'user': 0 }).exec()
            .then((follows) => {

                var follows_clean = [];
                follows.forEach((follow) => {
                    follows_clean.push(follow.followed);
                });
                console.log(follows_clean);
                return follows_clean;

            })
            .catch((error) => {
                return handleError(error);
            });

        var followed = await Follow.find({ "user": user_id }).select({ '_id': 0, '__v': 0, 'followed': 0 }).exec()
            .then((follows) => {
                var follows_clean = [];
                follows.forEach((follow) => {
                    follows_clean.push(follow.user);
                });
                return follows_clean;

            })
            .catch((error) => {
                return handleError(error);
            });

        return {
            following: following,
            followed: followed,
        }
    } catch (e) {
        console.log(e);
    }


}





//usuarios que siguen a un usuario
function getFollowedUsers(req, res) {

    var userId = req.user.sub;
    var page = 1;
    if (req.params.id && req.params.page) {
        userId = req.params.id;
    }

    if (req.params.page) {
        page = req.params.page;
    }

    var itemsPerPage = 4;
    Follow.find({ followed: userId })
        .populate('user')
        .paginate(page, itemsPerPage, (err, follows, total) => {
            if (err) return res.status(500).send({ message: 'Error en el servidor' });
            if (!follows) {
                return res.status(404).send({ message: 'No te está siguiendo ningun usuario' });
            }

            followUserId(req.user.sub).then((value) => {
                return res.status(200).send({
                    total: total,
                    pages: Math.ceil(total / itemsPerPage),
                    follows,
                    users_followed:value.followed,
                    users_following:value.following
                });
            });

        });
}

function getMyFollows(req, res) {
    var userId = req.user.sub;
    var find = Follow.find({ user: userId });

    if (req.params.followed) {
        find = Follow.find({ followed: userId });
    }

    find.populate('user followed').exec((error, follows) => {

        if (error) return res.status(500).send({ message: 'Error en el servidor' });

        if (!follows) return res.status(404).send({ message: 'No tienes follows' });

        return res.status(200).send({ follows });
    });


}



module.exports = {
    saveFollow,
    deleteFollow,
    getFollowingUsers,
    getFollowedUsers,
    getMyFollows,
}