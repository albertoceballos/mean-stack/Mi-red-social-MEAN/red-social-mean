'use strict'

var express=require('express');

var messageController=require('../controllers/messageController');
var middleware_auth=require('../middlewares/authenticated');

var api=express.Router();

api.post('/message',middleware_auth.ensureAuth, messageController.saveMessage);
api.get('/my-messages/:page?',middleware_auth.ensureAuth,messageController.getReceivedMessages);
api.get('/messages/:page?',middleware_auth.ensureAuth,messageController.getEmittedMessages);
api.get('/unviewed-messages',middleware_auth.ensureAuth,messageController.getUnviewedMessages);
api.get('/set-viewed-messages',middleware_auth.ensureAuth,messageController.setViewedMessages);
api.delete('/message-remove/:id',middleware_auth.ensureAuth,messageController.deleteMessage);


module.exports=api;