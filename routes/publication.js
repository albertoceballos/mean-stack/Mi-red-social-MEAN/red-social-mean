'use strict'

var express=require('express');
var PublicationController=require('../controllers/PublicationController');
var middleware_auth=require('../middlewares/authenticated');

var multipart=require('connect-multiparty');
var md_upload=multipart({uploadDir:'./uploads/publications'});

var api=express.Router();

api.post('/publication',middleware_auth.ensureAuth,PublicationController.savePublication);
api.get('/publications/:page?',middleware_auth.ensureAuth,PublicationController.getPublications);
api.get('/publications-user/:id/:page?',middleware_auth.ensureAuth,PublicationController.getPublicationsUser);
api.get('/publication/:id',middleware_auth.ensureAuth,PublicationController.getPublication);
api.delete('/publication-delete/:id',middleware_auth.ensureAuth,PublicationController.deletePublication);
api.post('/upload-image-pub/:id',[middleware_auth.ensureAuth,md_upload],PublicationController.uploadImage);
api.get('/get-image-pub/:imageFile',PublicationController.getImageFile);

module.exports=api;