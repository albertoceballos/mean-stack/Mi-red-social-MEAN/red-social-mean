'use strict'

var express = require('express');
var UserController = require('../controllers/userController');

var middleware_auth = require('../middlewares/authenticated');

var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/users' });

var api = express.Router();

api.get('/pruebas', middleware_auth.ensureAuth, UserController.pruebas);
api.post('/register', UserController.saveUser);
api.post('/login', UserController.loginUser);
api.get('/user/:id', middleware_auth.ensureAuth, UserController.getUser);
api.get('/users/:page', middleware_auth.ensureAuth, UserController.getUsers);
api.put('/user-update/:id', middleware_auth.ensureAuth, UserController.updateUsers);
api.post('/user-image-upload/:id', [middleware_auth.ensureAuth, md_upload], UserController.uploadImage);
api.get('/get-image-user/:imageFile',UserController.getImageFile);
api.get('/get-counters/:id?',middleware_auth.ensureAuth,UserController.getCounters);


module.exports = api;

